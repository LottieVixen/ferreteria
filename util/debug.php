<?php
/*
  PURPOSE: classes solely for debugging purposes
  HISTORY:
    2016-12-22 started, with lap-warming by Kestra
    2017-01-02 fcRowArray has now been replaced by fcDataRow_array
*/

trait ftInstanceCounter {
    private static $nInstance = 0;
    
    protected function ConstructInstance() {
	self::$nInstance++;
	return self::$nInstance;
    }
    protected function InstanceCount() {
	return self::$nInstance;
    }
}

/*::::
  SAMPLE USAGE:
    $oTrace = new fcStackTrace();
    echo $oTrace->RenderAllRows();
*/  
class fcStackTrace extends fcDataRow_array {

    public function __construct(exception $e=NULL) {
	if (is_null($e)) {
	    $this->LoadCurrentTrace(1);
	} else {
	    $this->SetAllRows($e->getTrace());
	}
    }

    protected function LoadCurrentTrace($nRemove=0) {
	// get the backtrace array
	$ar = debug_backtrace();
	// knock off the first n items (calls within this class)
	for ($idx = 0; $idx < $nRemove; $idx++) {
	    array_shift($ar);
	}
	$this->SetAllRows($ar);
    }

    // ++ FIELD VALUES ++ //
    
    protected function FileSpec() {
	return $this->GetFieldValue('file');
    }
    protected function LineNumber() {
	return $this->GetFieldValue('line');
    }
    protected function FunctionName() {
	return $this->GetFieldValue('function');
    }
    protected function ClassName() {
	return $this->GetFieldValue('class');
    }
    protected function Object() {	// not sure exactly what this field means
	return $this->GetFieldValue('object');
    }
    protected function MethodTypeString() {	// '::' if static call, '->' if dynamic call
	return $this->GetFieldValue('type');
    }
    protected function ArgsArray() {
	return $this->GetFieldValue('args');
    }
    
    // -- FIELD VALUES -- //
    // ++ FIELD CALCULATIONS ++ //
    
    protected function ArgsString() {
	$out = NULL;
	foreach ($this->ArgsArray() as $arg) {
	    if (!is_null($out)) {
		$out .= ',';
	    }
	    if (is_scalar($arg)) {
		if (is_string($arg)) {
		    $out .= '"'.$arg.'"';
		} else {
		    $out .= $arg;
		}
	    } elseif (is_array($arg)) {
		$nElem = count($arg);
		$out .= "array[$nElem]";
	    } else {
		$out .= '(<b>'.get_class($arg).'</b> object)';
	    }
	}
	return $out;
    }
    protected function FunctionString() {
	return
	  $this->ClassName()
	  .$this->MethodTypeString()
	  .$this->FunctionName().'('.$this->ArgsString().')'
	  ;
    }
    
    // -- FIELD CALCULATIONS -- //
    // ++ UI OUTPUT ++ //
    
    public function RenderAllRows() {
	$out = "\n<table class=trace>"
	  ."\n<tr><th>file</th><th>L#</th><th>call</th></tr>"
	  ;
	$this->RewindRows();
	while ($this->NextRow()) {
	    $sRow = $this->RenderRow();
	    $out .= "\n<tr>$sRow</tr>";
	}
	$out .= "\n</table>";
	return $out;
    }
    // NOTE: For now, <tr></tr> is rendered by the caller.
    protected function RenderRow() {
	return "<td align=right>".$this->FileSpec()."</td>"
	  .'<td align=right><b>'.$this->LineNumber().'</b></td>'
	  .'<td>'.$this->FunctionString().'</td>'
	  ;
    }
    
    // -- UI OUTPUT ++ //
}